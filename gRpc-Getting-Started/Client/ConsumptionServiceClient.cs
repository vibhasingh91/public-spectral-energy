﻿using System;
using Grpc.Net.Client;
using System.Threading.Tasks;
using MyTimeSeries;
using Google.Protobuf.WellKnownTypes;
using System.Collections.Generic;
using System.Linq;

namespace Client
{
    public class ConsumptionServiceClient
    {
        public async Task<List<ElectricityConsumptionDataReply>> GetConsumptionServiceData()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new ElectricityConsumpter.ElectricityConsumpterClient(channel);
            var reply = await client.RequestElectricityConsumptionDataAsync(new UsageRequest() { DateTime = DateTime.UtcNow.ToTimestamp() }); ;
            return reply.CompleteData.ToList();
        }
    }
}
