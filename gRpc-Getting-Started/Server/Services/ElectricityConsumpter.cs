﻿using CsvHelper;
using CsvHelper.Configuration;
using Grpc.Core;
using MyTimeSeries;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Server.Services
{
    public class ElectricityConsumpter : MyTimeSeries.ElectricityConsumpter.ElectricityConsumpterBase
    {
        public override Task<HistoricElectricityConsumptionData> RequestElectricityConsumptionData(UsageRequest request, ServerCallContext context)
        {
            //Read the contents of CSV file.
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\meterusage.csv");
            TextReader reader = new StreamReader(path);
            var csvReader = new CsvReader(reader, System.Globalization.CultureInfo.InvariantCulture);
            csvReader.Configuration.RegisterClassMap<ElectricityMapper>();
            var records = csvReader.GetRecords<ElectricityConsumptionDataReply>();
            var data = new HistoricElectricityConsumptionData { CompleteData = { records } };
            return Task.FromResult(data);
        }

        public class ElectricityMapper : ClassMap<ElectricityConsumptionDataReply>
        {
            public ElectricityMapper()
            {
                Map(x => x.Time).Name("time").Index(0).Index(0);
                Map(x => x.Meterusage).Name("meterusage").Index(1);
            }
        }
    }
}


