﻿using System.Collections.Generic;
using Client;
using Microsoft.AspNetCore.Mvc;
using MyTimeSeries;

namespace UI
{
    public class UsageController : Controller
    {
        public IActionResult Index()
        {
            return View(Get());
        }

        public IEnumerable<ElectricityConsumptionDataReply> Get()
        {
            return new ConsumptionServiceClient().GetConsumptionServiceData().Result;
        }
    }
}
